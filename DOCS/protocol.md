# Communication protocol v2

__Date:__ 08/02/2016

__Status:__ Draft

__Author:__ Vojtech Vladyka

__Changelog__

 Date       | Changes
 ---------- | -------
 08/02/2016 | First draft is forked from version 1 from Glow project
 09/02/2016 | Reworked chaters, added CRC implementation and described protocol features. Added draft of state machine for receive.
 
Communication protocol is inspired by IP protocol version 4. Because it will be implemented on 8 bit devices and on simple networks, it have some simplifications.

## Packet description

### Frame

	
	            8 bit
	+-----------------------------+ \
	| Version 4b | Packet type 4b | |
	+-----------------------------+ |
	|      Source address         | |
	+-----------------------------+ |
	|    Destination address      | > 6B
	+-----------------------------+ |
	|         Length              | |
	+-----------------------------+ |
	|      CRC High byte          | |
	|      CRC Low byte           | |
	+-----------------------------+ /
	|                             |
	|    Payload - up to 250B     |
	|                             |
	+-----------------------------+

### Packet types

There can be up to 16 packet types. 

 Code | Binary | Describtion
 ---- | ------ | -----------
 0    | 0000   | Blank message. Just header without body. Used as Acknowledge message.
 1    | 0001   | Message with some payload. Response is expeceted.
 2    | 0010   | Response for message #1.
 8    | 1001   | Message like #1, but incomplete. Next packet will follow.
 9    | 1010   | Response like #2, but for #8. It means packet were received and now is awaited next part.
 10   | 1011   | Ending packet for packets of #8 type. Response is #2. 
 15   | 1111   | Error report.

### CRC Calculation

CRC is used CRC-CCIT with seed 0xFFFF. For calculation purposes is CRC fields set to 0, the CRC is calculated and then is inserted into frame. CRC is calculated over whole frame.

## Protocol features

 - Maximum packet length is fixed to 256B, e.g. 250B of payload
 - 8bit addressing
 - 16bit CRC
 - 16 packet types
 - No start and stop byte. First byte must be maskable to used version of protocol, then after receive of first 6B is known expected length and can be received rest of packet
 - No escaping

## Implementation

Possibly best implementation would is state machine

 1. Receive first 6 bytes
 2. Read 4th byte to find out expected length
 3. Receive (length-6) bytes, e.g. rest of packet
 4. During that check for timeout
 5. When received all bytes, save aside CRC, clear appropriate field in packet and calculate CRC. Compare it. If it is ok, continue. If it differs, throw error packet (packet type #15) and dump packet. Return to #1.
 6. Give message to next processing


